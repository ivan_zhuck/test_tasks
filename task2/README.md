Создаем таблицы и индексы:

```sql
CREATE TABLE `magazines` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `magazines` (`id`, `name`) VALUES
(2, 'Крокодил'),
(3, 'Максим'),
(1, 'Мурзилка');

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users_magazines` (
  `user_id` bigint(20) NOT NULL,
  `magazine_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `magazines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `birthday` (`birthday`);

ALTER TABLE `users_magazines`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `magazine_id` (`magazine_id`);

ALTER TABLE `magazines`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users_magazines`
  ADD CONSTRAINT `users_magazines_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_magazines_ibfk_2` FOREIGN KEY (`magazine_id`) REFERENCES `magazines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
```

Получаем  пользователей старше 30 лет только тех, кто подписан на журнал Мурзилка:

```sql
SELECT users.* FROM users 
  LEFT JOIN users_magazines ON users.id = users_magazines.user_id
  LEFT JOIN magazines ON users_magazines.magazine_id = magazines.id
  WHERE 
      users.birthday < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 YEAR))
    AND
      magazines.name = 'Мурзилка'
  GROUP BY users.id;
```

Получаем случайного 1-го пользователя из 5 млн, для отображения его в модуле “наш любимый читатель” на сайте (способ для того, кто любит ждать):

```sql
SELECT * FROM users ORDER BY RAND() LIMIT 1;
```

И способ для тех, кто ждать не любит:

```sql
SELECT * FROM users AS random_1
    JOIN (SELECT (RAND() * (SELECT MAX(id) FROM users)) AS id) AS random_2
    WHERE random_1.id >= random_2.id
    ORDER BY random_1.id ASC
    LIMIT 1;
```
