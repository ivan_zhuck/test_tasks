Zepto = require('zepto-browserify').Zepto;

(function($){

    $(document).ready(function(){

        var $number = $('#number');
        var $percent = $('#percent');
        var $result = $('#result');

        $('.form__input').on('keypress', function(e){
            var keyCode = e.which ? e.which : e.keyCode;
            if(keyCode < 48 || keyCode > 57){
                return false;
            }
            return true;
        }).on('keyup', function(e){
            var number = $number.val() * 1;
            var percent = $percent.val() * 1;

            if(percent > 100){
                percent = 100;
                $percent.val(100);
            }

            $result.text(parseFloat((number * 0.01 * percent).toFixed(2)));

            return true;
        });

    });

})(Zepto);