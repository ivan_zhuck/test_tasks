<?php

namespace App;

abstract class Delivery{

    /**
     * Delivery providers factory method
     *
     * @param string $ProviderName
     * @return Delivery
     * @throws \Exception
     */
    public static function provider(string $ProviderName): self
    {
        $class = __NAMESPACE__ . "\\DeliveryProviders\\". $ProviderName;
        if(!class_exists($class)){
            throw new \Exception('Delivery provider name is wrong.');
        }
        return new $class;
    }

    /**
     * Rounds the total cost to 2 decimal places
     *
     * @param $cost
     * @return float
     */
    protected function roundCost($cost){
        return round($cost, 2, PHP_ROUND_HALF_UP);
    }

    /**
     * Calculates the cost of delivery
     *
     * @param array $args
     * @return float
     */
    abstract public function calc(array $args = []): float;
}