<?php

namespace App;

/**
 * It's helper for through data validation
 *
 * Class DataValidation
 * @package App
 */
trait DataValidation{

    /**
     * Weight must be more than 0 and type of variable must be float or integer
     *
     * @param $weight
     * @return bool
     */
    private function validateWeight($weight):bool{
        if((is_float($weight) or is_int($weight)) and $weight > 0){
            return true;
        }
        return false;
    }

}