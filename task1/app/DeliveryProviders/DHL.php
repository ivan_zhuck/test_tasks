<?php

namespace App\DeliveryProviders;
use App\DataValidation;
use App\Delivery;

class DHL extends Delivery{

    use DataValidation;

    private $cost = 100;

    public function calc(array $args = []): float{
        if(isset($args['weight']) and $this->validateWeight($args['weight'])){
            return $this->roundCost($args['weight'] * $this->cost);
        }
        throw new \Exception('Weight is wrong.');
    }

}