<?php

namespace App\DeliveryProviders;
use App\DataValidation;
use App\Delivery;

class RussianPost extends Delivery{

    use DataValidation;

    private $cost = [
        'up_to_10_kg' => 100,
        'more_than_10_kg' => 1000
    ];

    public function calc(array $args = []):float{
        if(isset($args['weigh']) and $this->validateWeight($args['weight'])){
            if($args['weight'] <= 10){
                return $this->roundCost($args['weight']*$this->cost['up_to_10_kg']);
            }
            return $this->roundCost($args['weight']*$this->cost['more_than_10_kg']);
        }
        throw new \Exception('Weight is wrong.');
    }

}